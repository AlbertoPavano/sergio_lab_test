import pymysql
from random import choice


class DbHandler:

    DATABASE = {
        "db_host": "10.190.7.41",
        "db_port": "3306",
        "db_name": "invoice_generation",
        "db_user": "admin",
        "db_password": "r3dr4wk5"
    }

    def __init__(self):
        self.conn = pymysql.connect(host=self.DATABASE['db_host'], user=self.DATABASE['db_user'],
                                    password=self.DATABASE['db_password'])

    def _connect_random_db(self):
        self.conn = pymysql.connect(host=self.DATABASE['db_host'], user=self.DATABASE['db_user'],
                                    password=self.DATABASE['db_password'], db=self._random_selector("SHOW DATABASES"))

    def _random_selector(self,sql_query: str):
        with self.conn.cursor() as cursor:
            cursor.execute(sql_query)
            return choice(tuple(db[0] for db in cursor.fetchall()))

    def _table_selector(self):
        try:
            self._connect_random_db()
            return self._random_selector("SHOW TABLES")
        except IndexError:
            return self._table_selector()

    def random_query_result(self):
        table = self._table_selector()
        with self.conn.cursor(pymysql.cursors.DictCursor) as cursor:
            #cursor.execute(f"SELECT * FROM {table} LIMIT 1") From python 3.6
            cursor.execute(f"SELECT * FROM {table} LIMIT 1")
            query_res = cursor.fetchone()
            if not query_res:
                return self.random_query_result()
            self.conn.close()
            return query_res
