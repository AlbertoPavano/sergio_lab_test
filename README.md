# Sergio's test tool.

```
sudo apt install virtualenv
sudo apt install python3.7-dev
```
```
pip install -r requirements
```

or Docker

```
docker login https://dockerhub.via-vox.net:5000
docker build -t dockerhub.via-vox.net:5000/powwownow/lab-sergio:1 .
docker push dockerhub.via-vox.net:5000/powwownow/lab-sergio:1
```
