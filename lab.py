import json
from flask import Flask
from lib.db_handler import DbHandler


app = Flask(__name__)


@app.route('/', defaults={'path': ''})
@app.route('/<path:path>')
def status(path):
    db = DbHandler()
    query = db.random_query_result()
    json_query = json.dumps(query, indent=4, default=str)
    return json_query


if __name__ == '__main__':
    app.run(debug=True, port=2000)
