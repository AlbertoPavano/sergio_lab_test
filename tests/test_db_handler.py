import unittest
from lib import db_handler


class TestDBHandler(unittest.TestCase):

    def setUp(self):
        self.db = db_handler.DbHandler()

    def test_select_random_db(self):
        """its selects a random database from the list of databases @ PowWowNow server """
        cursor = self.db.conn.cursor()
        cursor.execute("SHOW DATABASES")
        db_list = tuple(db[0] for db in cursor.fetchall())
        self.assertGreater(len(db_list), 1)
        self.assertTrue(self.db._random_selector("SHOW DATABASES") in db_list)

    def test_connect_to_random_db(self):
        """it connects a random database from the list of databases @ PowWowNow server """
        first_conn = self.db.conn.db
        self.db._connect_random_db()
        second_conn = self.db.conn.db
        self.assertNotEqual(first_conn, second_conn)

    def test_table_selector(self):
        """it returns a list of tables from a random database @ PowWowNow Server"""
        self.assertTrue(self.db._table_selector())

    def test_random_query_result(self):
        self.assertTrue(self.db.random_query_result())









